from .keys import PEXELS_API_KEY, WEATHER_API_KEY
import requests


def get_location_photo(city, state):
    parameters = {"query": f"{city}, {state}", "page": 1}
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers, params=parameters)
    data = response.json()
    try:
        return {"picture_url": data["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_conference_location(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    parameters = {"q": f"{city},{state},US", "appid": WEATHER_API_KEY}
    response = requests.get(url, params=parameters)
    data = response.json()
    try:
        lat = data[0]["lat"]
        lon = data[0]["lon"]
        return {"lat": lat, "lon": lon}
    except (KeyError, IndexError):
        return None


def get_conference_weather(lon, lat):
    url = "https://api.openweathermap.org/data/2.5/weather"
    parameters = {"lon": lon, "lat": lat, "appid": WEATHER_API_KEY}
    response = requests.get(url, params=parameters)
    data = response.json()
    try:
        temp = ((data["main"]["temp"] - 273.15) * (9 / 5) + 32) // 1
        main = data["weather"][0]["main"]
        description = data["weather"][0]["description"]
        weather_info = f"{main}: {description}"
        return {"temp": str(temp) + "F", "weather_info": weather_info}
    except (KeyError, IndexError):
        return {"message": "Weather Unavailable"}
